jupyterhub:
  proxy:
    chp:
      resources:
        request:
          cpu: 1
          memory: "1Gi"
        limits:
          cpu: 4
          memory: "4Gi"
    nginx:
      resources:
        request:
          cpu: 1
          memory: "1Gi"
        limits:
          cpu: 4
          memory: "4Gi"
    lego:
      resources:
        request:
          cpu: 1
          memory: "1Gi"
        limits:
          cpu: 4
          memory: "4Gi"
    secretToken: "<YOURTOKEN>" #should be the random generated string, not from CILogon
    service:
      type: ClusterIP
    secretToken: 
  hub:
    db:
      pvc:
        storageClassName: rook-ceph-block
    extraConfig: |
        #get CILogon authorization from authority at : https://www.cilogon.org/oidc
      c.JupyterHub.authenticator_class = 'oauthenticator.cilogon.CILogonOAuthenticator'
      c.CILogonOAuthenticator.username_claim = 'email'
      c.CILogonOAuthenticator.oauth_callback_url = "https://<YOURHOST>.nautilus.optiputer.net/hub/oauth_callback"
      c.CILogonOAuthenticator.client_id = "<YOURID>"
      c.CILogonOAuthenticator.client_secret = "<YOURSECRET>"
      from kubespawner import KubeSpawner
      class MySpawner(KubeSpawner):
        cmd = ['/opt/conda/bin/jupyter-labhub']
        notebook_dir = '/home/jovyan'
        profile_form_template = """
          <script>
          // JupyterHub 0.8 applied form-control indisciminately to all form elements.
          // Can be removed once we stop supporting JupyterHub 0.8
          $(document).ready(function() {
              $('#kubespawner-profiles-list input[type="radio"]').removeClass('form-control');
          });
          </script>
          <style>
          /* The profile description should not be bold, even though it is inside the <label> tag */
          #kubespawner-profiles-list label p {
              font-weight: normal;
          }
          </style>

          <label for="gpus">GPUs</label>
          <input class="form-control input-lg" type="number" name="gpus" value="1" min="0" max="8"/>
          <br/>
          <label for="ram">Cores</label>
          <input class="form-control input-lg" type="number" name="cores" value="1" min="0" max="96"/>
          <br/>
          <label for="ram">RAM, GB</label>
          <input class="form-control input-lg" type="number" name="ram" value="8" min="1" max="512"/>
          <br/>
          <label for="gputype">GPU type</label>
          <select class="form-control input-lg" name="gputype">
            <option value="" selected="selected">Any</option>
            <option value="V100-SXM2-32GB">V100-SXM2-32GB</option>
            <option value="1080Ti">1080Ti</option>
            <option value="2080Ti">2080Ti</option>
            <option value="1080">1080</option>
            <option value="titan-x">titan-x</option>
            <option value="K40">K40</option>
            <option value="M40">M40</option>
            <option value="M4000">M4000</option>
          </select>
          <br/>
          <label for="shm">/dev/shm for pytorch</label> <input class="form-control" type="checkbox" name="shm">
          <br/>
          <label for="cephfs">Mount CephFS (if assigned)</label> <input class="form-control" type="checkbox" name="cephfs">
          <p>You can request assignment in <a href='https://rocket.nautilus.optiputer.net'>RocketChat</a></p>
          <br/>

          <div class='form-group' id='kubespawner-profiles-list'>
          <label>Image</label>
          {% for profile in profile_list %}
          <label for='profile-item-{{ loop.index0 }}' class='form-control input-group'>
              <div class='col-md-1'>
                  <input type='radio' name='profile' id='profile-item-{{ loop.index0 }}' value='{{ loop.index0 }}' {% if profile.default %}checked{% endif %} />
              </div>
              <div class='col-md-11'>
                  <strong>{{ profile.display_name }}</strong>
                  {% if profile.description %}
                  <p>{{ profile.description }}</p>
                  {% endif %}
              </div>
          </label>
          {% endfor %}
          </div>"""

        def options_from_form(self, formdata):
          cephfs_users = {"myazdani@gmail.com":"myazdani", "dmishin@ucsd.edu":"dimm", "scottlsellars@gmail.com":"connect", "jjgraham@ucsd.edu":"aerosols", "erglass@ucsd.edu":"newspaper", "jun.xiong1981@gmail.com":"geonex", "wex041@eng.ucsd.edu":"mlpc", "jburney@ucsd.edu":"aerosols", "begao@ucsd.edu":"begao", "dreiman@ucsc.edu":"dreiman", "s9hu@eng.ucsd.edu": "ucsd-haosulab"}

          if not self.profile_list or not hasattr(self, '_profile_list'):
              return formdata
          # Default to first profile if somehow none is provided
          selected_profile = int(formdata.get('profile', [0])[0])
          options = self._profile_list[selected_profile]
          self.log.debug("Applying KubeSpawner override for profile '%s'", options['display_name'])
          kubespawner_override = options.get('kubespawner_override', {})
          for k, v in kubespawner_override.items():
              if callable(v):
                  v = v(self)
                  self.log.debug(".. overriding KubeSpawner value %s=%s (callable result)", k, v)
              else:
                  self.log.debug(".. overriding KubeSpawner value %s=%s", k, v)
              setattr(self, k, v)

          gpus = int(formdata.get('gpus', [0])[0])
          setattr(self, "extra_resource_limits", {"nvidia.com/gpu": gpus})

          setattr(self, "mem_guarantee", formdata.get('ram', [0])[0]+"G")

          setattr(self, "cpu_guarantee", float(formdata.get('cores', [0])[0]))

          if formdata.get('gputype', [0])[0]:
            setattr(self, 'extra_pod_config', {
              'affinity': {
                  'nodeAffinity': {
                      'requiredDuringSchedulingIgnoredDuringExecution': {
                          'nodeSelectorTerms': [{
                              'matchExpressions': [{
                                'key': 'gpu-type',
                                'operator': 'In',
                                'values': formdata.get('gputype', [0])
                              }],
                          }],
                      },
                  },
              }
            })

          self.volume_mounts = [
            {
              'name': 'volume-{username}',
              'mountPath': '/home/jovyan',
            }
          ]
          self.volumes = [
            {
              'name': 'volume-{username}',
              'persistentVolumeClaim': {
                'claimName': 'claim-{username}'
              }
            }
          ]

          if formdata.get('shm', [0])[0]:
            self.volume_mounts.append({
                'name': 'dshm',
                'mountPath': '/dev/shm',
              })
            self.volumes.append({
                'name': 'dshm',
                'emptyDir': {'medium': 'Memory'}
              })


          if formdata.get('cephfs', [0])[0] and self.user.name in cephfs_users:
              self.volume_mounts.append({
                  'name': 'cephfs',
                  'mountPath': '/cephfs',
                })
              self.volumes.append({
                  'name': 'cephfs',
                  'flexVolume': {
                    'driver': 'ceph.rook.io/rook',
                    'fsType': 'ceph',
                    'options': {
                      'fsName': 'nautilusfs',
                      'clusterNamespace': 'rook',
                      'path': '/'+cephfs_users[self.user.name],
                      'mountUser': cephfs_users[self.user.name],
                      'mountSecret': 'ceph-fs-secret-'+cephfs_users[self.user.name]
                    }
                  }
                })

          return options

        profile_list = [
            {
                'display_name': 'Stable Python3.7, tensorflow 2, CUDA 10, R, Matlab',
                'default': 'true',
            },
            {
                'display_name': 'Tensorflow 1.14',
                'kubespawner_override': {
                  'image_spec': 'gitlab-registry.nautilus.optiputer.net/prp/jupyterlab:tf1',
                  'image_pull_policy': 'IfNotPresent',
                }
            },
            {
                'display_name': 'Latest image (testing)',
                'kubespawner_override': {
                  'image_spec': 'gitlab-registry.nautilus.optiputer.net/prp/jupyterlab:latest',
                  'image_pull_policy': 'Always',
                }
            },
        ]
      c.JupyterHub.spawner_class = MySpawner      
  auth:
    state:
      enabled: true
      cryptoKey: "<YOURTOKEN>"
    #admin:
    #  users:
    #    - dmishin@ucsd.edu
    #    - jjgraham@ucsd.edu

  prePuller:
    hook:
      enabled: false

  singleuser:
    cpu:
      limit: 2
      requests: 1
      guarantee: 1
    memory:
      limit: 8G
      requests: 1G
      guarantee: 1G
    extraResource:
      limits: {}
      guarantees: {}
      requests: {}
    defaultUrl: "/lab"
    image:
      name: gitlab-registry.nautilus.optiputer.net/prp/jupyterlab
      tag: 3b5d41ec
    cloudMetadata:
      enabled: true
    storage:
      capacity: 5Gi
      dynamic:
        storageClass: rook-ceph-block
  ingress:
    enabled: true
    hosts:
      - <YOURHOST>.nautilus.optiputer.net
