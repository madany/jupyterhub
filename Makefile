upgrade:
	helm --tiller-namespace=jupyterlab upgrade --install --namespace=jupyterlab nautilus-hub nautilus-hub

buildrelease:
	cd tensorflow-notebook && \
	docker build -t gitlab-registry.nautilus.optiputer.net/prp/jupyterlab .

pushdocker:
	docker push gitlab-registry.nautilus.optiputer.net/prp/jupyterlab

inithelm:
	helm repo add jupyterhub https://jupyterhub.github.io/helm-chart/
	helm repo update
	helm init --client-only --service-account tiller --tiller-namespace=jupyterhub